package eu.mysterygames.buildffa.core;

import eu.mysterygames.buildffa.events.*;
import eu.mysterygames.buildffa.utils.*;
import eu.mysterygames.mystery.utils.MySQL;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Robin Kroczek on Jan, 2020 at 00:36
 */
public class BuildFFA extends JavaPlugin {


    public static final String prefix = "§7┃ §cBuildFFA §7» ";
    public static final String noperm = prefix + "§cDazu bist du nicht berechtigt!";
    public static final String noplayer = prefix + "§cDu musst ein Spieler sein";

    private static BuildFFA instance;

    public static Location spawn, death, save;
    public static int deathCoins = -5, killCoins = 20;

    private static Config config;
    public static HashMap<UUID, User> users = new HashMap<>();

    public boolean reconnectSQL = false;

    public Voting currentVoting = null;
    private long lastVoting = 0;

    @Override
    public void onEnable() {
        super.onEnable();

        instance = this;
        config = new Config(this);

        register();
        connectSQL();

        Bukkit.getConsoleSender().sendMessage("Plugin: " + prefix + getName() + " enabled!");
    }

    private void register() {

        if(config.contains("buildffa.spawn")) spawn = config.getLocation("buildffa.spawn");
        else Bukkit.getConsoleSender().sendMessage(prefix + "§cEs wurde noch kein Spawn gesetzt!");

        if(config.contains("buildffa.death")) death = config.getLocation("buildffa.death");
        else Bukkit.getConsoleSender().sendMessage(prefix + "§cEs wurde noch kein Todespunkt gesetzt!");

        if(config.contains("buildffa.save")) save = config.getLocation("buildffa.save");
        else Bukkit.getConsoleSender().sendMessage(prefix + "§cEs wurde noch keine Save-Location gesetzt!");

        getServer().getPluginManager().registerEvents(new EventJoinQuit(), this);
        getServer().getPluginManager().registerEvents(new EventDamage(), this);
        getServer().getPluginManager().registerEvents(new EventOther(), this);
        getServer().getPluginManager().registerEvents(new EventBuild(), this);
        getServer().getPluginManager().registerEvents(new EventDeath(), this);
        getServer().getPluginManager().registerEvents(new EventMove(), this);
        getServer().getPluginManager().registerEvents(new EventInteract(), this);
        getServer().getPluginManager().registerEvents(new EventProjectile(), this);

        KitType.setCurrent(KitType.RUSHER);


        spawn.getWorld().setDifficulty(Difficulty.EASY);
    }

    private void connectSQL() {
      /*  MySQL.host = "localhost";
        MySQL.database = "mysterygames";
        MySQL.username = "root";
        MySQL.password = "";
				con.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS BuildFFA (id INT PRIMARY KEY AUTO_INCREMENT, UUID VARCHAR(100), RUSHER VARCHAR(300), BOW VARCHAR(300));");
        MySQL.port = "3306";*/
        MySQL.createTable("BuildFFA", "UUID VARCHAR(100)", "RUSHER VARCHAR(100)", "BOW VARCHAR(100)");

    }

    @Override
    public void onDisable() {
        super.onDisable();
        Bukkit.getConsoleSender().sendMessage("Plugin: " + prefix + getName() + " disabled!");

        for(User user : users.values()) {
            user.quit();
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        Player p = null;
        User u = null;
        if(sender instanceof Player) {
            p = (Player) sender;
            u = users.get(p.getUniqueId());
        }

        if(command.getName().equalsIgnoreCase("buildffa")) {
            if(args.length == 2) {
                if(args[0].equalsIgnoreCase("set")) {
                    if(p != null) {
                        if(p.hasPermission("buildffa.setup")) {
                            if(args[1].equalsIgnoreCase("spawn")) {
                                spawn = p.getLocation();
                                config.setLocation("buildffa.spawn", spawn);
                                p.sendMessage(prefix + "Du hast den Spawn gesetzt.");
                            }else if(args[1].equalsIgnoreCase("death")) {
                                death = p.getLocation();
                                config.setLocation("buildffa.death", death);
                                p.sendMessage(prefix + "Du hast den Todespunkt gesetzt.");
                            }else if(args[1].equalsIgnoreCase("save")) {
                                save = p.getLocation();
                                config.setLocation("buildffa.save", save);
                                p.sendMessage(prefix + "Du hast die Save-Location gesetzt.");
                            }
                        }else {
                            p.sendMessage(noperm);
                        }
                    }else{
                        sender.sendMessage(noplayer);
                    }
                }
            }
        }else if(command.getName().equalsIgnoreCase("build")){
            if(p != null) {
                if(p.hasPermission("buildffa.build")) {
                    u.setBuild(!u.isBuild());
                }else {
                    p.sendMessage(noperm);
                }
            }else {
                sender.sendMessage(noplayer);
            }
        }else if(command.getName().equalsIgnoreCase("save")) {
            if(p != null) {
                if(p.getLocation().getY() > BuildFFA.save.getY()){
                    u.kits.replace(KitType.getCurrent(), p.getInventory().getContents());
                    String kit = KitType.getCurrent().toString().substring(0, 1).toUpperCase() + KitType.getCurrent().toString().substring(1).toLowerCase();

                    p.sendMessage(prefix + "Du hast dein Inventar für das Kit §b" + kit + " §7gespeichert.");
                }else {
                    p.sendMessage(prefix + "§cDu musst ab Spawn sein um dein Inventar zu speichern!");
                }
            }else {
                sender.sendMessage(noplayer);
            }
        }else if(command.getName().equalsIgnoreCase("vote") && p != null) {
            if(args.length == 1) {
                if(currentVoting == null) {
                    if(System.currentTimeMillis() - lastVoting >= 1000*60*7) { // 10 Mins (2 Min voting delay)
                        if(args[0].equalsIgnoreCase("Rusher") || args[0].equalsIgnoreCase("Bow") || args[0].equalsIgnoreCase("Rod") || args[0].equalsIgnoreCase("EP"))
                        try {
                            KitType type = KitType.valueOf(args[0].toUpperCase());

                            if(!type.equals(KitType.getCurrent())) {
                                setCurrentVoting(new Voting(type));
                            }else {
                                String kit = type.toString().substring(0, 1).toUpperCase() + type.toString().substring(1).toLowerCase();
                                p.sendMessage(prefix + "§7Das derzeitige Kit ist bereits §b" + kit + "§7!");
                            }
                        }catch (IllegalArgumentException e) {
                            p.sendMessage(prefix + "§cDas angegebene Kit konnte nicht gefunden werden!");
                            p.sendMessage(prefix + "§7Bitte verwende /vote <Rusher/Bow/Rod/EP>!");
                        }
                    }else {
                        p.sendMessage(prefix + "§cBitte warte einen Moment bis das Kit erneut gewechselt werden kann!");
                    }
                }else {
                    if(args[0].equalsIgnoreCase("yes")) {
                        this.currentVoting.vote(p, true);
                    }else if(args[0].equalsIgnoreCase("no")) {
                        this.currentVoting.vote(p, false);
                    }else {
                        p.sendMessage(prefix + "Es läuft bereits eine Abstimmung zur Kitwechslung!");
                    }

                }
            }else {
                p.sendMessage(prefix + "§7Bitte verwende /vote <Rusher/Bow/Rod/EP>!");
            }
        }

        return true;
    }

    public static BuildFFA getInstance() {
        return instance;
    }

    public static void broadcast(String message) {
        for(Player all : Bukkit.getOnlinePlayers()) {
            all.sendMessage(prefix + message);
        }
    }

    public static void sendActionBar(Player p, String msg) {
        IChatBaseComponent icbc = IChatBaseComponent.ChatSerializer.a("{\"test\": \"" + msg + "\"}");
        PacketPlayOutChat bar = new PacketPlayOutChat(icbc, (byte)2);

        ((CraftPlayer)p).getHandle().playerConnection.sendPacket(bar);
    }

    public void setCurrentVoting(Voting currentVoting) {
        this.currentVoting = currentVoting;

        lastVoting = System.currentTimeMillis();
    }

    public static void updateRanking() {
        Bukkit.getScheduler().runTaskTimerAsynchronously(BuildFFA.getInstance(), new Runnable() {
            @Override
            public void run() {
                for(User u : BuildFFA.users.values()) {

                }
            }
        }, 0, 20*60*5);
    }
}
