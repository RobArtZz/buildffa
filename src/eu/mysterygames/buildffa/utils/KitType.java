package eu.mysterygames.buildffa.utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Robin Kroczek on Jan, 2020 at 02:09
 */
public enum KitType {

    RUSHER, BOW, ROD, EP;

    public static KitType current;

    public static void setCurrent(KitType current) {
        KitType.current = current;
    }

    public static KitType getCurrent() {
        return current;
    }

    public static ItemStack[] getInventoryFromType(KitType type) {
        ItemStack[] items = new ItemStack[35];

        items[0] = new ItemBuilder(Material.GOLD_SWORD).setUnbreakable(true).addEnchant(Enchantment.DAMAGE_ALL, 1).build();

        items[5] = new ItemBuilder(Material.SANDSTONE).setAmount(64).build();
        items[6] =  new ItemBuilder(Material.SANDSTONE).setAmount(64).build();
        items[7] =  new ItemBuilder(Material.SANDSTONE).setAmount(64).build();
        items[8] = new ItemBuilder(Material.SANDSTONE).setAmount(64).build();

        if(type.equals(KitType.RUSHER)) {
            items[1] = new ItemBuilder(Material.STICK).addEnchant(Enchantment.KNOCKBACK, 1).build();
        }else if(type.equals(KitType.ROD)) {
            items[1] = new ItemBuilder(Material.FISHING_ROD).setUnbreakable(true).build();
        }else if(type.equals(KitType.BOW)) {
            items[1] = new ItemBuilder(Material.BOW).setUnbreakable(true).addEnchant(Enchantment.ARROW_INFINITE, 1).build();
            items[17] = new ItemBuilder(Material.ARROW).build();
        }else if(type.equals(KitType.EP)) {
            items[1] = new ItemBuilder(Material.ENDER_PEARL).setAmount(4).build();
        }

        return items;
    }
}
