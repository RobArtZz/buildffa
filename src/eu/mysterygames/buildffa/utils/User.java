package eu.mysterygames.buildffa.utils;

import dev.lukasl.meow.commons.entities.UserEntity;
import eu.mysterygames.buildffa.core.BuildFFA;
import eu.mysterygames.mystery.utils.MySQL;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by Robin Kroczek on Jan, 2020 at 00:39
 */
public class User {

    private Player p;
    private boolean build;

    private int kills = 0;
    private int deaths = 0;

    private int killStreak = 0;

    private long lastDamageByEntity = 0, lastEnderPearl = 0;
    private Player lastDamager = null;

    public HashMap<KitType, ItemStack[]> kits = new HashMap<>();

    private UserEntity userEntity = null;

    public User(Player player) {
        this.p = player;

        createUserEntityWeilArgsDummIst();
    }



    private void createUserEntityWeilArgsDummIst() {
        userEntity = new UserEntity();

        userEntity.setCredits(0);

    }

    public Player getPlayer() {
        return p;
    }

    public void join() {
        p.setHealth(20);
        p.setLevel(0);

        loadKits();

        p.teleport(BuildFFA.spawn);
        p.setGameMode(GameMode.SURVIVAL);
        fillInventory();

        sendScoreboard();

    }

    public void quit() {

        if(getLastDamager() != null && System.currentTimeMillis() - getLastDamageByEntity() <= 1000 * 5) {
            if(BuildFFA.users.containsKey(getLastDamager().getUniqueId())) {
                User damager = BuildFFA.users.get(getLastDamager().getUniqueId());

                damager.addKill();

                if(damager.getPlayer().getLocation().getY() > BuildFFA.death.getY()) {
                    damager.getPlayer().setHealth(20);
                }

                damager.getPlayer().sendMessage(BuildFFA.prefix + "Du hast §b" + p.getName() + " §7getötet!");
                damager.getPlayer().playSound(damager.getPlayer().getLocation(), Sound.NOTE_PIANO, 1.0f, 1.0f);
            }
            addDeath();
        }

        saveKits();
    }

    public void fillInventory() {
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);

        p.getInventory().setContents(kits.get(KitType.getCurrent()));

        p.getInventory().setHelmet(new ItemBuilder(Material.LEATHER_HELMET).setUnbreakable(true).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build());
        p.getInventory().setChestplate(new ItemBuilder(Material.CHAINMAIL_CHESTPLATE).setUnbreakable(true).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).addEnchant(Enchantment.PROTECTION_PROJECTILE, 1).build());
        p.getInventory().setLeggings(new ItemBuilder(Material.LEATHER_LEGGINGS).setUnbreakable(true).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build());
        p.getInventory().setBoots(new ItemBuilder(Material.LEATHER_BOOTS).setUnbreakable(true).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build());

        p.updateInventory();
    }

    public void sendScoreboard() {
        Scoreboard.sendScoreboard(getPlayer(), "§6§lM§e§lystery§6§lG§e§lames.eu", "§a", "§7Kills:", "§7➜ §e" + getKills(), "§n", "§7Deaths:", "§r§7➜ §e" + getDeaths(),
                "§4", "§7Coins:", "§f§7➜ §e" + getCoins(), "§5", "§7Rang:",
                "§7➜ " + (p.hasPermission("tag.admin") ? "§4Admin" :  p.hasPermission("tag.builder") ? "§2Builder" :
                         p.hasPermission("tag.dev") ? "§bDeveloper" :
                                p.hasPermission("tag.mod") ? "§cModerator" : p.hasPermission("tag.sup") ? "§9Supporter" :
                                        p.hasPermission("tag.youtuber") ? "§5Youtuber" : p.hasPermission("tag.vip") ? "§eVIP" : "§aSpieler"),
                "§2", "§7TS:", "§7➜ §eMysteryGames.eu");
    }

    public void setBuild(boolean build) {
        this.build = build;

        if(build) {
            p.getInventory().clear();
            p.getInventory().setArmorContents(null);
            p.setGameMode(GameMode.CREATIVE);
            p.sendMessage(BuildFFA.prefix + "Du kannst nun bauen.");
        }else {
            p.setGameMode(GameMode.SURVIVAL);
            p.teleport(BuildFFA.spawn);
            fillInventory();
            p.sendMessage(BuildFFA.prefix + "Du kannst nun nicht mehr.");
        }
    }

    public boolean isBuild() {
        return build;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getKills() {
        return kills;
    }

    public int getKillStreak() {
        return killStreak;
    }

    private void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    private void setKills(int kills) {
        this.kills = kills;
    }

    private void setKillStreak(int killStreak) {
        this.killStreak = killStreak;

        if(getKillStreak() != 0 && (getKillStreak() % 5 == 0 || getKillStreak() == 3)) {
            BuildFFA.broadcast("Der Spieler §b" + p.getName() + " §7hat einen §b" + getKillStreak() + "er §7Killstreak erreicht!");

            addCoins(getKillStreak());
            getPlayer().sendMessage(BuildFFA.prefix + "Du hast §b" + getKillStreak() + " §7Coins erhalten!");

            if(p.getLocation().getY() < BuildFFA.save.getY()){
                p.getInventory().addItem(new ItemBuilder(Material.GOLDEN_APPLE).build());
            }
        }

        getPlayer().setLevel(getKillStreak());
    }

    public void addKill() {
        addCoins(BuildFFA.killCoins);
        getPlayer().sendMessage(BuildFFA.prefix + "Du hast §b" + BuildFFA.killCoins + " §7Coins erhalten!");

        setKills(getKills() + 1);
        setKillStreak(getKillStreak() + 1);


        if(KitType.getCurrent().equals(KitType.EP)) {
            getPlayer().getInventory().addItem(new ItemBuilder(Material.ENDER_PEARL).build());
        }

        sendScoreboard();
    }

    public void addDeath() {
        setDeaths(getDeaths() + 1);
        setKillStreak(0);
        setLastEnderPearl(0);

        if(getCoins() > 5) {
            addCoins(BuildFFA.deathCoins);
            p.sendMessage(BuildFFA.prefix + "Du hast §b" + Math.abs(BuildFFA.deathCoins) + " §7Coins verloren!");
        }

        sendScoreboard();
    }

    public int getCoins() {
        return getUserEntity().getCredits();
    }

    public void addCoins(int coins) {
        getUserEntity().setCredits(getCoins() + coins);
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }


    public void loadKits() {
        /*

        SLOT:ID:AMOUNT,.....


         */
        try {
            //TODO: CHECK IF EXISTS
            ResultSet rs = MySQL.getResult("SELECT * FROM BuildFFA WHERE UUID='" + getPlayer().getUniqueId().toString() + "';");

            if(rs == null) return;
            if(rs.next()) {
                for(KitType type : KitType.values()) {
                    String string = rs.getString(type.toString());
                    String[] splitted = string.split(",");

                    ItemStack[] items = new ItemStack[35];

                    for(int i = 0; i<splitted.length; i++) {
                        String s = splitted[i];
                        int slot = Integer.parseInt(s.split(":")[0]);
                        int id = Integer.parseInt(s.split(":")[1]);
                        int amount = Integer.parseInt(s.split(":")[2]);
                        ItemBuilder builder = new ItemBuilder(Material.getMaterial(id)).setAmount(amount);

                        if(builder.getMaterial().equals(Material.BOW)) builder.setUnbreakable(true).addEnchant(Enchantment.ARROW_INFINITE, 1);
                        else if(builder.getMaterial().equals(Material.STICK)) builder.addEnchant(Enchantment.KNOCKBACK, 1);
                        else if(builder.getMaterial().equals(Material.FISHING_ROD)) builder.setUnbreakable(true);
                        else if(builder.getMaterial().equals(Material.ENDER_PEARL)) builder.setAmount(4);
                        else if(builder.getMaterial().equals(Material.GOLD_SWORD)) builder.setUnbreakable(true).addEnchant(Enchantment.DAMAGE_ALL, 1);

                        items[slot] = builder.build();

                    }

                    kits.put(type, items);
                }
            }else {
                kits.put(KitType.RUSHER, KitType.getInventoryFromType(KitType.RUSHER));
                kits.put(KitType.BOW, KitType.getInventoryFromType(KitType.BOW));
                kits.put(KitType.EP, KitType.getInventoryFromType(KitType.EP));
                kits.put(KitType.ROD, KitType.getInventoryFromType(KitType.ROD));
                MySQL.uptade("INSERT INTO BuildFFA (UUID, RUSHER, BOW, ROD, EP) VALUES ('" + p.getUniqueId().toString()+"', '"+convertKitToString(kits.get(KitType.RUSHER))+"', '"+
                        convertKitToString(kits.get(KitType.BOW)) + "', '" + convertKitToString(kits.get(KitType.ROD)) + "', '" + convertKitToString(kits.get(KitType.EP)) + "')");
            }

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveKits() {
        MySQL.uptade("UPDATE BuildFFA SET RUSHER='" + convertKitToString(kits.get(KitType.RUSHER)) + "', BOW='" + convertKitToString(kits.get(KitType.BOW)) + "', " +
                "ROD='" + convertKitToString(kits.get(KitType.ROD)) + "', EP='" + convertKitToString(kits.get(KitType.EP)) + "' WHERE UUID='" + getPlayer().getUniqueId().toString() + "';");
    }

    private String convertKitToString(ItemStack[] items) {
        StringBuilder stringBuilder = new StringBuilder();

        boolean first = true;

        for(int i = 0; i<items.length; i++) {
            ItemStack stack = items[i];
            if(stack != null) {

                if(!first) {
                    stringBuilder.append(",");
                }

                stringBuilder.append(i).append(":").append(stack.getType().getId()).append(":").append(stack.getAmount());
                first = false;
            }
        }

     //   System.out.println(stringBuilder.toString());
        return stringBuilder.toString();
    }

    public void pushStats() {

    }

    public void loadStats() {
        ResultSet rs = MySQL.getResult("SELECT * FROM ");
    }

    public void setLastDamageByEntity(long lastDamageByEntity) {
        this.lastDamageByEntity = lastDamageByEntity;
    }

    public void setLastDamager(Player lastDamager) {
        this.lastDamager = lastDamager;
    }

    public long getLastDamageByEntity() {
        return lastDamageByEntity;
    }

    public Player getLastDamager() {
        return lastDamager;
    }

    public long getLastEnderPearl() {
        return lastEnderPearl;
    }

    public void setLastEnderPearl(long lastEnderPearl) {
        this.lastEnderPearl = lastEnderPearl;
    }
}
