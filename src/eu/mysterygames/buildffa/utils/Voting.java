package eu.mysterygames.buildffa.utils;

import eu.mysterygames.buildffa.core.BuildFFA;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Robin Kroczek on Jan, 2020 at 14:57
 */
public class Voting {

    private KitType type;

    private BukkitTask task;
    private int count = 0;

    private int voteYes = 0, voteNo = 0;
    private HashMap<UUID, Boolean> alreadyVoted = new HashMap<>();

    public Voting(KitType type) {
        this.type = type;

        TextComponent yes = new TextComponent();
        TextComponent no = new TextComponent();
        yes.setText(" §8[§aJa§8]");
        no.setText(" §8[§cNein§8]");
        yes.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§aJa§7, das Kit wechseln!").create()));
        no.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cNein§7, das Kit nicht wechseln!").create()));
        yes.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/vote yes"));
        no.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/vote no"));

        TextComponent question = new TextComponent(BuildFFA.prefix + "Möchtest du das Kit wechseln?");

        String kit = type.toString().substring(0, 1).toUpperCase() + type.toString().substring(1).toLowerCase();

        this.task = Bukkit.getScheduler().runTaskTimerAsynchronously(BuildFFA.getInstance(), new Runnable() {
            @Override
            public void run() {

                if(count < 1) {
                    BuildFFA.broadcast("Ein Spieler möchte das Kit zu §b" + kit + " §7wechseln");
                    for(Player all : Bukkit.getOnlinePlayers()) {
                        all.spigot().sendMessage(question, yes, no);
                    }

                }else if(count == 1){
                    if(voteNo > voteYes) {
                        BuildFFA.broadcast("Die Mehrheit hat dafür entschieden das Kit nicht zu wechseln!");
                    }else {
                        BuildFFA.broadcast("Die Mehrheit hat dafür entschieden das Kit zu §b" + kit + " §7zu wechseln!");
                        KitType.setCurrent(getType());

                        for(User all : BuildFFA.users.values()) {
                            all.fillInventory();
                        }
                    }

                    BuildFFA.getInstance().setCurrentVoting(null);
                    task.cancel();
                }

                count++;
            }
        }, 0, 20 * 60);
    }


    public KitType getType() {
        return type;
    }

    public HashMap<UUID, Boolean> getAlreadyVoted() {
        return alreadyVoted;
    }

    public void vote(Player p, boolean change) {
        // DELETE IF EXISTS
        getAlreadyVoted().remove(p.getUniqueId());

        if(change) {
            voteYes++;
            p.sendMessage(BuildFFA.prefix + "Du hast für einen Kitwechsel abgestimmt!");
        }else {
            voteNo++;
            p.sendMessage(BuildFFA.prefix + "Du hast gegen einen Kitwechsel abgestimmt!");
        }
        alreadyVoted.put(p.getUniqueId(), change);
    }
}
