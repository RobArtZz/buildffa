package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.core.BuildFFA;
import eu.mysterygames.buildffa.utils.ItemBuilder;
import eu.mysterygames.buildffa.utils.User;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Robin Kroczek on Jan, 2020 at 01:01
 */
public class EventBuild implements Listener {

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        User u = BuildFFA.users.get(e.getPlayer().getUniqueId());

        if(!u.isBuild()) {
            if(p.getLocation().getY() >= BuildFFA.save.getY()) {
                e.setCancelled(true);
            }else {
                if(e.getItemInHand().getType().equals(Material.SANDSTONE)) {
                    e.getItemInHand().setAmount(64);
                    p.updateInventory();
                }

                if(!e.getBlock().getType().equals(Material.LADDER) && !e.getBlock().getType().equals(Material.WEB)) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            e.getBlock().setType(Material.REDSTONE_BLOCK);
                        }
                    }.runTaskLater(BuildFFA.getInstance(), 20*3);
                }
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        e.getBlock().setType(Material.AIR);
                        e.getBlock().getWorld().spigot().playEffect(e.getBlock().getLocation().clone().add(0.5,0,0.5), Effect.EXTINGUISH);
                    }
                }.runTaskLater(BuildFFA.getInstance(), 20*5);

            }
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        User u = BuildFFA.users.get(e.getPlayer().getUniqueId());

        if(!u.isBuild()) {
            e.setCancelled(true);
        }
    }
}
