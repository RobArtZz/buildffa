package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.core.BuildFFA;
import eu.mysterygames.buildffa.utils.User;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * Created by Robin Kroczek on Jan, 2020 at 01:36
 */
public class EventDeath implements Listener {


    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        User u = BuildFFA.users.get(p.getUniqueId());
        u.setLastDamager(null);

        e.setDeathMessage(null);
        e.setKeepInventory(true);
        e.setDroppedExp(0);

        u.addDeath();

        if(p.getKiller() != null) {
            User killer = BuildFFA.users.get(p.getKiller().getUniqueId());
            killer.addKill();

            killer.getPlayer().sendMessage(BuildFFA.prefix + "Du hast §b" + p.getName() + " §7getötet!");
            killer.getPlayer().playSound(killer.getPlayer().getLocation(), Sound.NOTE_PIANO, 1.0f, 1.0f);

            if(killer.getPlayer().getLocation().getY() > BuildFFA.death.getY()) {
                killer.getPlayer().setHealth(20);
            }

            p.sendMessage(BuildFFA.prefix + "Du wurdest von §b" + killer.getPlayer().getName() + " §7getötet!");
        }else {

            p.sendMessage(BuildFFA.prefix + "Du bist gestorben!");
        }
        p.playSound(p.getLocation(), Sound.NOTE_BASS, 1.0f, 1.0f);


        Bukkit.getScheduler().runTaskLater(BuildFFA.getInstance(), new Runnable() {

            @Override
            public void run() {
                p.spigot().respawn();
            }
        }, 10);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        e.setRespawnLocation(BuildFFA.spawn);
        User u = BuildFFA.users.get(e.getPlayer().getUniqueId());
        u.fillInventory();

    }
}
