package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.core.BuildFFA;
import eu.mysterygames.buildffa.utils.User;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by Robin Kroczek on Jan, 2020 at 00:47
 */
public class EventDamage implements Listener {


    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if((e.getDamager() instanceof Player) && e.getEntity() instanceof Player) {
            Player p = (Player) e.getDamager();
            if(p.getLocation().getY() < BuildFFA.save.getY()) {
                Player hitted = (Player) e.getEntity();
                User u = BuildFFA.users.get(hitted.getUniqueId());
                u.setLastDamager(p);
                u.setLastDamageByEntity(System.currentTimeMillis());
            }else {
                e.setCancelled(true);
            }

        }else if(e.getDamager() instanceof Arrow || e.getDamager() instanceof FishHook) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if(e.getCause().equals(EntityDamageEvent.DamageCause.FALL) ||e.getCause().equals(EntityDamageEvent.DamageCause.VOID)) {
       // if(!e.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK) && !e.getCause().equals(EntityDamageEvent.DamageCause.PROJECTILE)) {
            e.setCancelled(true);
        }
    }
}
