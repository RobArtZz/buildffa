package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.core.BuildFFA;
import eu.mysterygames.buildffa.utils.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

/**
 * Created by Robin Kroczek on Jan, 2020 at 00:45
 */
public class EventOther implements Listener {

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        User u = BuildFFA.users.get(e.getPlayer().getUniqueId());

        if(!u.isBuild()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onMonserSpawn(EntitySpawnEvent e) {
        if(!(e.getEntity() instanceof Player)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBedEnter(PlayerBedEnterEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onWeather(WeatherChangeEvent e) {
        e.setCancelled(true);
    }

}
