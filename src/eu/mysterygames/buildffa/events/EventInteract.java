package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.core.BuildFFA;
import eu.mysterygames.buildffa.utils.User;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by Robin Kroczek on Jan, 2020 at 16:46
 */
public class EventInteract implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        User u = BuildFFA.users.get(p.getUniqueId());

        if(!u.isBuild()) {
            if(e.getItem() != null && e.getItem().getType() != null) {
                if(e.getItem().getType().equals(Material.ENDER_PEARL)) {
                    if(p.getLocation().getY() >= BuildFFA.spawn.getY()) {
                        e.setCancelled(true);
                        p.updateInventory();
                    }else if(System.currentTimeMillis() - u.getLastEnderPearl() <= 3000) {
                        e.setCancelled(true);
                        p.updateInventory();
                        p.sendMessage(BuildFFA.prefix + "§cBitte warte bis du erneut eine Enderperle nutzen kannst!");
                    }else {
                        u.setLastEnderPearl(System.currentTimeMillis());
                    }
                    return;
                }

                if(!e.getItem().getType().equals(Material.SANDSTONE) && !e.getItem().getType().equals(Material.BOW) && !e.getItem().getType().equals(Material.GOLDEN_APPLE) && !e.getItem().getType().equals(Material.FISHING_ROD)) {
                    e.setCancelled(true);
                }
            }
        }
    }
}
