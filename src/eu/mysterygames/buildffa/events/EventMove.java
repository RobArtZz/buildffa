package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.core.BuildFFA;
import eu.mysterygames.buildffa.utils.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Created by Robin Kroczek on Jan, 2020 at 01:12
 */
public class EventMove implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        User u = BuildFFA.users.get(p.getUniqueId());
        if(!u.isBuild()) {
            if(!p.isDead() && p.getLocation().getY() <= BuildFFA.death.getY()) {
                p.setHealth(0);
            }
        }
    }
}
