package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.core.BuildFFA;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;

/**
 * Created by Robin Kroczek on Jan, 2020 at 19:30
 */
public class EventProjectile implements Listener {

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent e) {
        if(e.getEntity() instanceof Arrow) {
            e.getEntity().remove();
        }else if(e.getEntity() instanceof EnderPearl) {
            EnderPearl pearl = (EnderPearl) e.getEntity();

            if(e.getEntity().getLocation().getY() >= BuildFFA.save.getY()) {
                e.getEntity().remove();
                e.getEntity().setBounce(false);
            }
        }
    }

    @EventHandler
    public void onProjectileLaunch(ProjectileLaunchEvent e) {
        if(e.getEntity().getShooter() instanceof Player) {
            Player p = (Player) e.getEntity().getShooter();
            if(p.getLocation().getY() >= BuildFFA.save.getY()) {
                e.setCancelled(true);
            }
        }
    }
}
