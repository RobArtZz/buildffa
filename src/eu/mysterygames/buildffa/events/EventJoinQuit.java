package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.core.BuildFFA;
import eu.mysterygames.buildffa.utils.User;
import eu.mysterygames.mystery.utils.MySQL;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Robin Kroczek on Jan, 2020 at 00:40
 */
public class EventJoinQuit implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        Player p = e.getPlayer();

        if(BuildFFA.getInstance().reconnectSQL) {
            if(MySQL.isConnected()) {
                MySQL.disconnect();
            }
            MySQL.connect();
            BuildFFA.getInstance().reconnectSQL = false;
        }

        User u = new User(p);
        BuildFFA.users.put(p.getUniqueId(), u);

        u.join();
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        Player p = e.getPlayer();

        if(BuildFFA.getInstance().reconnectSQL) {
            if(MySQL.isConnected()) {
                MySQL.disconnect();
            }
            MySQL.connect();
            BuildFFA.getInstance().reconnectSQL = false;
        }

        User u = BuildFFA.users.get(p.getUniqueId());

        u.quit();
    }

}
